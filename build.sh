#!/bin/sh
gcc -fPIC -I/usr/local/include -c src/keystone_pam.c
sudo ld -x --shared -L/usr/local/lib -o /usr/lib/keystone_pam.so keystone_pam.o -lpam -lcurl

g++ -o pam_test src/test.c -lpam 

rm keystone_pam.o
