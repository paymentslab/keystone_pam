#include <sys/param.h>

#include <pwd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <security/pam_modules.h>
#include <security/pam_appl.h>

#include <curl/curl.h>

#ifndef _OPENPAM
static char password_prompt[] = "Password:";
#endif

#ifndef PAM_EXTERN
#define PAM_EXTERN
#endif

#define BUFSIZE 256

struct data_struct {
        char *mem;
        size_t size;
};

static size_t 
write_mem_callback(void *contents, size_t size, size_t nmemb, void *chunk)
{
	size_t realsize = size * nmemb;
	struct data_struct *data        = (struct data_struct *) chunk;
        
	data->mem = realloc(data->mem, data->size + realsize + 1);
        
	memcpy(&(data->mem[data->size]), contents, realsize);
	data->size += realsize;
	data->mem[data->size] = 0;

	return realsize;
}

PAM_EXTERN int
pam_sm_authenticate(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{
#ifndef _OPENPAM
	struct pam_conv *conv;
	struct pam_message msg;
	const struct pam_message *msgp;
	struct pam_response *resp;
#endif
	struct passwd *pwd;
	const char *user;
	char *crypt_password, *password;
	int pam_err, retry;
	int i;
	long http_code;
	
	CURL *curl;
	CURLcode res;
	
	struct curl_slist *headerlist=NULL;
	static const char buf[] = "Content-Type: application/json";
	static const char auth1[] = "{\"auth\": {\"passwordCredentials\": {\"username\": \"";
	static const char auth2[] = "\", \"password\": \"";
	static const char auth3[] = "\"}}}";
	
	struct data_struct chunk;
        
	int len;
	char *auth;
	
	char url[BUFSIZE];
	int has_url = 0;
		
	/* get parameters */
	for (i=0; i<argc; i++) {
		if (strncmp(argv[i], "url=", 4) == 0) {
			strlcpy(url, argv[i]+4, sizeof(url));
			has_url = 1;
		}
	}
	
	if (has_url == 0) {
		return (PAM_SYSTEM_ERR);
	}
	
	/* identify user */
	if ((pam_err = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS)
		return (pam_err);

	/* get password */
#ifndef _OPENPAM
	pam_err = pam_get_item(pamh, PAM_CONV, (const void **)&conv);
	if (pam_err != PAM_SUCCESS)
		return (PAM_SYSTEM_ERR);
	msg.msg_style = PAM_PROMPT_ECHO_OFF;
	msg.msg = password_prompt;
	msgp = &msg;
#endif
	for (retry = 0; retry < 3; ++retry) {
#ifdef _OPENPAM
		pam_err = pam_get_authtok(pamh, PAM_AUTHTOK,
		    (const char **)&password, NULL);
#else
		resp = NULL;
		pam_err = (*conv->conv)(1, &msgp, &resp, conv->appdata_ptr);
		if (resp != NULL) {
			if (pam_err == PAM_SUCCESS)
				password = resp->resp;
			else
				free(resp->resp);
			free(resp);
		}
#endif
		if (pam_err == PAM_SUCCESS)
			break;
	}
	if (pam_err == PAM_CONV_ERR)
		return (pam_err);
	if (pam_err != PAM_SUCCESS)
		return (PAM_AUTH_ERR);

	/* compare passwords */
	len = snprintf(NULL, 0, "%s%s%s%s%s", auth1, user, auth2, password, auth3);
	auth = malloc((len + 1) * sizeof(char));
	snprintf(auth, len + 1, "%s%s%s%s%s", auth1, user, auth2, password, auth3);
	
	chunk.mem = malloc(1);
	chunk.size = 0;
        
	curl_global_init(CURL_GLOBAL_ALL);
        
	curl = curl_easy_init();
	headerlist = curl_slist_append(headerlist, buf);

	pam_err = PAM_AUTH_ERR;
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, auth);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_mem_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
          
		res = curl_easy_perform(curl);
                
		if (res != CURLE_OK) 
			fprintf(stderr, "curl_easy_perform failedL%s\n", curl_easy_strerror(res));

		/* TODO: check more */
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
		if (http_code == 200) {
			pam_err = PAM_SUCCESS;
		}		
		/*if (strstr(chunk.mem, "token")) {
			pam_err = PAM_SUCCESS;
		}*/

		curl_easy_cleanup(curl);
                
		if (chunk.mem)
			free(chunk.mem);
               
		curl_slist_free_all (headerlist);
	}

	free(auth);

#ifndef _OPENPAM
	free(password);
#endif
	return (pam_err);
}

PAM_EXTERN int
pam_sm_setcred(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_acct_mgmt(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_open_session(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_close_session(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_chauthtok(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SERVICE_ERR);
}

#ifdef PAM_MODULE_ENTRY
PAM_MODULE_ENTRY("pam_keystone");
#endif
